load("Data/german.rda")

summary(german)
str(german)

plot(Amount ~ Duration, german)
abline(lm(Amount ~ Duration, german))

## For the Gaussian family (used by default), glm() gives the same
## "result" (coefficient estimates) as lm(), but different summaries:

ml <- lm(Amount ~ Duration, data = german)
summary(ml)
mg <- glm(Amount ~ Duration, data = german)
summary(mg)

?glm

## By default, R gives the *working* residuals and the fitted mean
## values (hence, on the "response scale".

plot(residuals(mg) ~ fitted(mg),
     xlab = expression(hat(y)[i]),
     ylab = expression(r[i]))
abline(0, 0, lty = 2)

## We follow Müller to investigate the relation between Class and Age.

boxplot(Age ~ Class, data = german)
plot(Class ~ Age, data = german)

## Remember that this is a *spinogram*, which has the x axis transformed
## non-linearly (according to the distribution of the x values).

## Visualizing a binary response according to a continuous explanatory
## variable x is generally hard.  One common recipe is to use "empirical
## logits": one divides the range of x into suitable intervals (e.g.,
## deciles) and then with $n_i$ and $y_i$ the numbers of cases and
## "successes" (e.g., response 1 for 0/1 data), one plots the empirical
## log-odds
##    log ( y_i + 0.5 / (n_i + 0.5 - y_i) )
## e.g. against the mid-points of the intervals.  This is particularly
## useful when subsequently illustrating the fit of a logistic
## regression model.

## More generally, one can consider the "smoothed" success rates
##    (y_i + 0.5) / (n_i + 1)
## where the extra terms being added ensure staying away from 0 and 1,
## and visualize these on the response scale or the predictor scale.
## We simply use intervals of length five:
breaks <- c(seq(15, 70, by = 5), Inf)
tab <- with(german, table(Class, cut(Age, breaks)))
p <- (tab["good", ] + 0.5) / (colSums(tab) + 1)
x <- head(breaks, -1L) + 2.5            # midpoints
plot(x, p, ylim = c(0, 1))
lines(lowess(x, p))
?lowess

## Or, use a conditional density plot, which is a smoothed variant of
## the above approach.
cdplot(Class ~ Age, data = german)

## Logistic regression of Class on Age:
m1 <- glm(Class ~ Age, family = binomial(), data = german)
summary(m1)

## To visualize the predictions, we can do something like:
cdplot(Class ~ Age, data = german)
ages <- with(german, seq(from = min(Age), to = max(Age)))
p1 <- predict(m1, data.frame(Age = ages), type = "response")
# lines(ages, 1 - p1, type = "l", col = "red")
lines(ages, p1, type = "l", col = "red")

## Should we have used a different link?
m2 <- glm(Class ~ Age, family = binomial(link = "probit"), data = german)

## Which one is better?  AICs are basically the same.  Predictions:
p2 <- predict(m2, data.frame(Age = ages), type = "response")

lines(ages, 1 - p2, type = "l", col = "blue")
lines(ages, p2, type = "l", col = "blue")

## Or compare predictions directly and/or fitted values directly:
op <- par(mfcol = c(1, 2))
plot(predict(m1), predict(m2),
     xlab = "logit", ylab = "probit", main = "Predictions")
plot(fitted(m1), fitted(m2),
     xlab = "logit", ylab = "probit", main = "Fitted values")
par(op)

## Is it better to include higher-order effects of Age?
m2 <- glm(Class ~ Age + I(Age^2), family = binomial(), data = german)
## Note that the I() is needed!  Alternatively,
m2 <- update(m1, . ~ . + I(Age^2))
summary(m2)

## Which one is better?
anova(m1, m2)

## Hmm, why does this not give p-values by default?  Bug or feature?
anova(m1, m2, test = "LRT")

## Maybe also a third-order (cubic) effect?
m3 <- update(m2, . ~ . + I(Age^3))
summary(m3)
anova(m1, m2, m3, test = "LRT")
## No.

## Maybe it would be better to form age groups?
breaks <- c(seq(from = 18, to = 68, by = 5), 75)

m4 <- glm(Class ~ cut(Age, breaks), family = binomial(), data = german)
## Is this better than e.g. m1?  As these are not nested we cannot use
## analysis of deviance to decide.  Comparing AICs:
AIC(m1)
AIC(m4)
## suggests that m1 is better.

## Adding more predictors:
m2 <- glm(Class ~ Amount + Age, family = binomial(), data = german)
summary(m2)
anova(m1, m2)
anova(m1, m2, test = "LRT")

## And quadratic effects for these:
m3 <- update(m2, . ~ . + I(Age^2))
m4 <- update(m3, . ~ . + I(Amount^2))
## This shows an interesting "problem": the AIC of m4 is smaller than
## that of m3:
c(m3 = AIC(m3), m4 = AIC(m4))
c(m3 = BIC(m3), m4 = BIC(m4))
## but the model is not found to be significantly better:
anova(m3, m4, test = "LRT")
## So which one should we choose?
## Here, "easy" because Amount^2 is only marginally significant and
## Amount is not any more.

## This can also be illustrated via confidence intervals:
confint(m4)
## Significance requires that zero is not contained in the interval.

## So instead, let us try adding another predictor:
m4 <- update(m3, . ~ . + Status_of_checking_account)
summary(m4)
anova(m1, m2, m3, m4, test = "LRT")
c(m3 = AIC(m3), m4 = AIC(m4))
## *REALLY* better now ...

## But is this a good model?
## Let us predict into the class with higher probability.  I.e., given a
## set of explanatory variables x,
##   Predict "bad" <=> P(Class = "bad" | x) >= 1/2
## For our fitted models, we can get these predictions by rounding the
## fitted values.

## Predictive performance:
with(german, table(Class, round(fitted(m4))))
## So this is actually not too good, as more than two thirds of the bad
## ones are predicted as good.

## Maybe we need more predictors?

## Heuristic to seek a better model with stepwise selection.
mf <- glm(Class ~ ., family = binomial(), data = german)
ms <- step(mf)

## Predictive performance:
table(german$Class, round(fitted(ms)))
## Much better ...
## But what did we actually come up with?
summary(ms)
## Argh.  Isn't this too complicated?

## The predictive performance of credit scoring models is typically
## visualized using ROC curves:
require("ROCR")
pred <- prediction(fitted(ms), german$Class, c("good", "bad"))
plot(performance(pred, "acc"))
plot(performance(pred, "tpr", "fpr"))
abline(0, 1, lty = 2)

## Modeling of contingency tables using Poisson regression.

tab <- xtabs(~ Class + Status_of_checking_account + Job, data = german)
plot(tab)
levels(german$Job)
levels(german$Status_of_checking_account)


mp1 <- glm(Freq ~ ., family = quasipoisson(), data = as.data.frame(tab))
mp2 <- update(mp1, . ~ . + Class : Job + Class : Status_of_checking_account)
mp3 <- update(mp1, . ~ . + .^2)
anova(mp1, mp2, mp3, test = "LRT")

